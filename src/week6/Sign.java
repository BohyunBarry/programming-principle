/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week6;

/**
 *
 * @author D00169978
 */
public class Sign {
     public static void main(String[] args) {
        
        egg();
        sign("STOP");
        sign(" GO ");
        sign("WAIT");
        
    }
    //draw the brigde figure.
    public static void eight(){
        eggTop();
        eggBottom();
        eggTop();
        eggBottom();
        System.out.println();
    }
    //Draw the eight figure.
    
    // Draws the top half of an an egg figure.
    public static void eggTop() {
        System.out.println("  ______");
        System.out.println(" /      \\");
        System.out.println("/        \\");
    }
    
    // Draws the bottom half of an egg figure.
    public static void eggBottom() {
        System.out.println("\\        /");
        System.out.println(" \\______/");
    }
    
    // Draws a complete egg figure.
    public static void egg() {
        eggTop();
        eggBottom();
        System.out.println();
    }
    // Draws a sign figure.
    public static void sign(String message) {
        eggTop();
        System.out.println("|  "+message+"  |");
        eggBottom();
        System.out.println();
    }
    
    
    
    
}
