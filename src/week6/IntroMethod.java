/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week6;

/**
 *
 * @author D00169978
 */
public class IntroMethod 
{
    public static void main(String[] args) {
        
        egg();
        goSign();
        stopSign();
        
    }
    //draw the brigde figure.
    public static void eight(){
        eggTop();
        eggBottom();
        eggTop();
        eggBottom();
        System.out.println();
    }
    //Draw the eight figure.
    
    // Draws the top half of an an egg figure.
    public static void eggTop() {
        System.out.println("  ______");
        System.out.println(" /      \\");
        System.out.println("/        \\");
    }
    
    // Draws the bottom half of an egg figure.
    public static void eggBottom() {
        System.out.println("\\        /");
        System.out.println(" \\______/");
    }
    
    // Draws a complete egg figure.
    public static void egg() {
        eggTop();
        eggBottom();
        System.out.println();
    }
    // Draws a teacup figure.
    public static void goSign() {
        eggTop();
        System.out.println("|   GO   |");
        eggBottom();
        System.out.println();
    }
    
    // Draws a stop sign figure.
    public static void stopSign() {
        eggTop();
        System.out.println("|  STOP  |");
        eggBottom();
        System.out.println();
    }
    
    


}
