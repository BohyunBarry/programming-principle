/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week5;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class workd {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();

        //declare
        int game;
        int gamecount=1;
        System.out.print("Please enter how many times of game you want to play?: ");
        game = keyboard.nextInt();
        
        while(gamecount <=game)
        {
            gamecount++;
            
        int start;
        int end;
        System.out.print("Please enter starting number: ");
        start = keyboard.nextInt();
        System.out.print("Please enter ending number: ");
        end = keyboard.nextInt();
        int random = start + rand.nextInt((end - start) + 1);
        int number;
        int count = 1;
        int hint;
        int chance;
        String guess;
        String Hint = null;
        
        System.out.print("Please enter number of chance: ");
        chance = keyboard.nextInt();
        System.out.print("Please guess a number between "+start+"-"+end+" :");
        number = keyboard.nextInt();
        
        if(number < start || number > end)
        {
            count=count-1;
            System.out.println("number is not valid");
        }
        
        if (number > random) 
        {
            hint = number - random;
            guess = "guess down";
        } 
        else 
        {
            hint = random - number;
            guess = "guess up";
        }

        if (hint >= 5) 
        {
            Hint = "Cold";
        } 
        else if (hint >= 2 && hint <= 4) 
        {
            Hint = "Warm";
        }
        else if (hint == 1) 
        {
            Hint = "Hot";
        }
       
       
        while (number != random && count < chance) 
        {
            System.out.println("Hint:" + Hint + "\t" + guess);
            System.out.println("Wrong!Guess again: ");
            number = keyboard.nextInt();
            if(number < start || number > end)
            {
                 count=count-1;
                 System.out.println("number is not valid");
             }
            
            if (number > random) {
                hint = number - random;
                guess = "guess down";
            } else {
                hint = random - number;
                guess = "guess up";
            }

            if (hint >= 5) {
                Hint = "Cold";
            } else if (hint >= 2 && hint <= 4) {
                Hint = "Warm";
            } else if (hint == 1) {
                Hint = "Hot";
            }
            

            count++;
        }

        if (number == random) 
        {
            System.out.println("Correct!!Answer= " + random);
        }
        else 
        {
            System.out.println("No more chances!Answer= " + random);
        }
    }
    }
}
