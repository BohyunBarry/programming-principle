/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week5;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class Workc 
{
     public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        Random rand = new Random();

        //declare
        int random = 1 + rand.nextInt(20);
        int number;
        int count = 1;
        int hint;
        System.out.print("Please guess a number between 1-20: ");
        number = keyboard.nextInt();

        hint = number - random;
        hint = Math.abs(hint);
        
        if (hint >= 5) 
        {
            System.out.println("Cold");
        } 
        else if (hint >= 2 && hint <= 4) 
        {
            System.out.println("Warm");
        } 
        else if(hint==1)
        {
            System.out.println("Hot");
        }

        while (number != random && count < 5) 
        {
            System.out.println("Wrong!Guess again: ");
            number = keyboard.nextInt();
            
           hint = number - random;
           hint = Math.abs(hint);

            if (hint >= 5) 
            {
                System.out.println("Cold");
            } 
            else if (hint >= 2 && hint <= 4) 
            {
                System.out.println("Warm");
            }
            else if(hint==1)
            {
                System.out.println("Hot");
            }
            
            count++;
        }

        if (number == random) {
            System.out.println("Correct!!Answer= " + random);
        } else {
            System.out.println("No more chances!Answer= " + random);
        }
    }
}
