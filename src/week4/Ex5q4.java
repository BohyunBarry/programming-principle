/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
/**
 *
 * @author D00169978
 */
public class Ex5q4 
{
   public static void main(String[] args) throws IOException

    {
          // link File object with a file on disk
        
        File inputFile = new File("input.txt");
        
        //link Scanner object with the File
        Scanner input = new Scanner(inputFile);
        int number;
        String highest= null;
        String name = null;
        int max=0;
        int countfirst=0;
        int countsecondg1=0;
        int countsecondg2=0;
        int countpass=0;
        int countfail=0;
        int countOOR=0;
        boolean valid;
        
        while (input.hasNext())          // more values in file?
        {
            valid = true;
            number = input.nextInt();    // read next integer
            name = input.nextLine();
            System.out.println("Name: " + name);
            System.out.println("The mark is " + number);

           
            if (number >= 70 && number <= 100) 
            {
                countfirst++;
            } else if (number >= 60 && number <= 69) 
            {
                countsecondg1++;
            } else if (number >= 50 && number <= 59) 
            {
                countsecondg2++;
            } else if (number >= 40 && number <= 49) 
            {
                countpass++;
            } else if (number >= 0 && number <= 39) 
            {
                countfail++;
            } else {
                valid = false;
                countOOR++;
            }
            if (number >= max && valid) 
            {
                max = number;
                highest = name;
            }

       
    }   
      
      
        System.out.println("The person with the highest mark is: "+highest+"\nMark is: "+ max);
	System.out.println("The number of First grade= "+countfirst);
        System.out.println("The number of Second Grade1 = "+countsecondg1);
        System.out.println("The number of Second Grade2= "+countsecondg2);
        System.out.println("The number of Pass grade= "+countpass);
        System.out.println("The number of Fail grade= "+countfail);
        System.out.println("The number of Out of Range= "+countOOR);
    }
}
