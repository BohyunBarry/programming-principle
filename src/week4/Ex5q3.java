/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Ex5q3 
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        //declare
        double smallTpopulation= 30435;
        double bigTpopulation=37816;
        final double GROWRATE=0.0225;
        final double DROPRATE=0.015;
        double totalsmallT;
        double totalbigT;
        
        int count=1;
        
        //input
        do 
        {
            System.out.println("year"+count);
            
            totalsmallT = smallTpopulation * GROWRATE;
            smallTpopulation = smallTpopulation + totalsmallT;
            
            totalbigT = bigTpopulation * DROPRATE;
            bigTpopulation = bigTpopulation - totalbigT;
            
            System.out.println(smallTpopulation);
            System.out.println(bigTpopulation);

            count++;
        } while (smallTpopulation < bigTpopulation );
        
        System.out.println(count);
        System.out.println(smallTpopulation);
        System.out.println(bigTpopulation);
    }
}
