/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week4;

import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class Ex5q5 
{
     public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        final int MIN = 1;
        final int MAX = 10;     
        
        int value;     
                
        System.out.print("Enter integer value:");         
        value = keyboard.nextInt(); 
        
        while (value < MIN || value > MAX)           // Note condition
        {         
            System.out.println("Error - integer value not in correct range");  
            System.out.println("The range is "+MIN+ " - "+MAX );
            System.out.print("Enter integer value:");         
            value = keyboard.nextInt();             
        }          
        
        System.out.println("Value entered in valid range: " + value);

        
    }
}
