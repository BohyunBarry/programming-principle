/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week4;

import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class Ex5q1 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        //declare
        int number = 1;
        double sum = 0;
        int count = 0;
        double average;
        int max ;
        int min ;

        //input
        System.out.print("Please enter number(-1 to quit) :");
        number = keyboard.nextInt();
        max=number;
        min=number;
        while (number != -1) {
            //process
            count++;
            sum = sum + number;

            if (number >= max) {
                max = number;
            }

            if (number <= min) {
                min = number;
            }
            //input
            System.out.print("Please enter number (-1 to quit):");
            number = keyboard.nextInt();

        }
        average = sum / count;

        System.out.println("how many numbers the user had entered? " + count);
        System.out.println("the average of number=" + average);
        System.out.println("the maximum of number=" + max);
        System.out.println("the minimum of number=" + min);
    }
}
