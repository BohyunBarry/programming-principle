/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Reference;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class Examples 
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        int max;
        int num1;
        int num2;
        int count=0;
        System.out.println("Please enter a number");
        num1=keyboard.nextInt();
        
        Random rand=new Random();
        num2=2+rand.nextInt(6);
        
        
        
        max=getMax(1,3,7);
        System.out.println("The maximum number is "+max);
        max=getMax(2,num1,8);
        System.out.println("The maximum number is "+max);
        max=getMax(1,3,num2);
        System.out.println("The maximum number is "+max);
        max=getMax(1,12,7);
        System.out.println("The maximum number is "+max);
        max=getMax(9,3,7);
        System.out.println("The maximum number is "+max);
    }
    
    public static int getMax(int value1, int value2, int value3)
    {
        int result;
        if(value1>value2 && value1>value3)
        {
         result=value1;
        }
        else if(value2> value2 && value2> value3)
        {
            result=value2;
        }
        else
        {
            result=value3;
        }
        return result;
    }
}
