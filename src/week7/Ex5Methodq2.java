/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week7;

/**
 *
 * @author D00169978
 */
public class Ex5Methodq2 
{
    public static void main(String[] args) 
    {
        double area;
        area=areaCircle(5);
        System.out.printf("The area of circle is %.2f%n",area);
        area=areaCircle(15);
        System.out.printf("The area of circle is %.2f%n",area);
        area=areaCircle(18);
        System.out.printf("The area of circle is %.2f%n",area);
    }
    
    public static double areaCircle(int radius)
    {
        double calculate;
        calculate=(Math.PI)*Math.pow(radius,2);
        return calculate;        
    }
}
