/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week7;

/**
 *
 * @author D00169978
 */
public class Ex5Methodq3 
{
    public static void main(String[] args) 
    {
        int max;
        max=getMax(1,3,7);
        System.out.println("The maximum number is "+max);
        max=getMax(2,4,8);
        System.out.println("The maximum number is "+max);
        max=getMax(1,3,10);
        System.out.println("The maximum number is "+max);
        max=getMax(1,12,7);
        System.out.println("The maximum number is "+max);
        max=getMax(9,3,7);
        System.out.println("The maximum number is "+max);
    }
    
    public static int getMax(int value1, int value2, int value3)
    {
        int result;
        if(value1>value2 && value1>value3)
        {
         result=value1;
        }
        else if(value2> value2 && value2> value3)
        {
            result=value2;
        }
        else
        {
            result=value3;
        }
        return result;
    }
}
