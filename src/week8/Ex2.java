/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week8;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Ex2 
{
    public static void main(String[] args)
   {
    Scanner keyboard = new Scanner(System.in);
    int numOfStudents = 0;  

    int[] marks;   // Declare array
    int best;
    int lowest;
    int total=0;
    double average;
    char grade;  
    
    // 1.	Get number of students
   System.out.println("Please enter number of students");
   numOfStudents = keyboard.nextInt();
     // Create array 
   marks = new int[numOfStudents]; // note: can use #students to allocate space
    
    // 2.	Input marks from user and store in array
   System.out.println("Please enter " + numOfStudents + " marks");
   for (int i = 0; i<marks.length; i++)
   {
      marks[i] = keyboard.nextInt();      
   }
    
     // 3.	Determine the best student mark 
       best = marks[0];
       for (int i = 1; i < marks.length; i++) {
           if (marks[i] > best) {
               best = marks[i];
           }
       }
       System.out.println("Best student mark obtained was:" + best + "\n");
       
       lowest = marks[0];      
   for (int i = 1; i < marks.length; i++)
   {      
     if (marks[i] < lowest)
       lowest = marks[i];
   }
   System.out.println("The lowest student mark obtained was:" +lowest+"\n");
        
   for(int i=0; i<marks.length;i++)
   {
       total=total+marks[i];
   }
   average=(double)total/marks.length;
   System.out.println("The average mark awarded is " +average+"\n");
   
     // 4.	Determine and display grades for each student
   for (int i = 0; i < marks.length; i++)
   {
      if (marks[i] >= 85)
      {
         grade = 'A';
      }
      else if (marks[i] >= 70)	
      {
         grade = 'B';
      }
      else if (marks[i] >= 55)
      {
         grade = 'C';
      }
      else if (marks[i] >= 40)
      {
         grade = 'D';
      }
      else
      {      
         grade = 'F';
      }
     System.out.println("Student " + i + " score is " + marks[i] +
                        " and grade is " + grade);

    
     }
   }
}
