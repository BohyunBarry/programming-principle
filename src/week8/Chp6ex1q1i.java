/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week8;


/**
 *
 * @author D00169978
 */
public class Chp6ex1q1i 
{
     public static void main(String[] args)
     {
        
       int[] list=new int[10];
       list[0]=1;
       list[2]=list[0]+list[1];
       list[9]=8;
       
       
       System.out.println(list[0]);
       System.out.println(list[2]);
       System.out.println(list[list.length-1]);
       System.out.println();
       
       //q1ii.
       for(int i=0;i<list.length;i++)
       {
           System.out.print(list[i]);
       }
       System.out.println();
       //q1iii
       
       for(int number:list)
       {
           System.out.print(number);
       }
       System.out.println();
       
        //qiv
       int total=0;
       double average;
        for(int number:list)
       {
           total=total+number;
       }
        average=(double)total/list.length;
        System.out.println("Average= "+average);
        
        //q1v
        boolean flag=true;
        int max=0;
        int i=0;
        while(i<list.length && flag==true)
        {
           if(list[i]>list[i+1])
           {
               flag=false;
           }
           
           i++;
        }
        
        if(flag)
        {
        System.out.println("It's in ascending order");
        }
        else
        {
            System.out.println("It's not in ascending order");
        }
    }
}
