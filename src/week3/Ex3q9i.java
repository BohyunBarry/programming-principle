/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week3;

import java.util.Random;

/**
 *
 * @author User
 */
public class Ex3q9i
{ 
    public static void main(String[] args) 
 {
     Random rand=new Random();
     int num;
     String day;
     final int DAYS=5;
     boolean friday=false;
     
     for (int i = 1; i <= DAYS; i++) 
     {
         num = 1 + rand.nextInt(7);
         if (num == 1) 
         {
             day = "Monday";
         } 
         else if (num == 2)
         {
             day = "Tuesday";
         } 
         else if (num == 3) 
         {
             day = "Wednesday";
         } 
         else if (num == 4) 
         {
             day = "Thursday";
         }
         else if (num == 5) 
         {
             day = "Friday";
             friday=true;
         } 
         else if (num == 6) 
         {
             day = "Saturday";
         } 
         else 
         {
             day = "Sunday";
         }
         System.out.println("Today is " + day);
     }
    if (friday)
    {
        System.out.println("If friday is one of those days? Yes");    
    }
    else
    {
        System.out.println("If friday is one of those days? No"); 
    }
 }
    
}
