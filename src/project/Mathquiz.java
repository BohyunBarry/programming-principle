/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

 public class Mathquiz 
{ 
    public static void main(String[] args) 
 {
    int choice1;
    String [] menu1={"1)Question from file", "2)Random Questions", "3)Finish"};
    choice1=displayMenu(menu1);
    while(choice1 !=3)
    {
        if (choice1==1)
        {
            playGameFileData();
        }
        else
        {
            playGameRandomData();
        }
         choice1=displayMenu(menu1);
    }
 }

   public static int displayMenu(String[] option)
   {
      int choice;
       System.out.println(Arrays.toString(option));
      choice=getValidInt(0,(option.length - 1),"Enter choice");
      return choice;
       
   }
   
   public static int getValidInt(int min,int max, String prompt)
   {
       Scanner keyboard=new Scanner(System.in);
       int value;
       System.out.println(prompt);
       value=keyboard.nextInt();
       
       while(value<(min+1) || value>(max+1) )
       {
           System.out.println("Error-integer value not in correct range");
           System.out.println(prompt);
           value=keyboard.nextInt();
       }
       return value;
   }
   public static int getRandomInt(int low, int high)
   {   
       Random rand=new Random();
       int num;
       num=low + rand.nextInt((high-low)+1);
       return num;
   }
   public static void playGameFileData()
   {}
   public static void playGameRandomData()
   {
       int choice2;
       String operator = "+";
       int level =1;
       int game;
       String [] menu2={"1)Choose Question Type", "2)Choose Question Level", "3)Play Game", "4)Show player points", "5)End Quiz"};
       choice2=displayMenu(menu2);
            while(choice2 !=5)
            {
                if(choice2 == 1)
                { 
                    operator=getType();
                    choice2=displayMenu(menu2);
                }
                else if(choice2 == 2)
                { 
                    level=getLevel();
                    choice2=displayMenu(menu2);
                }
                else if(choice2 == 3)
                { 
                    playGame(level,operator); 
                }
                else
                { 
                    showPoints();
                }
            }}
   public static String getType()
   {
       int choice3;
       String operator;
       String [] menu3={"1)addition","2)subtraction","3)multiplication", "4)division"};
       choice3=displayMenu(menu3);
       
       if(choice3 ==1)
       {
          operator=question("+");
       }
       else if(choice3 ==2)
       {
           operator=question("-");
       }
       else if(choice3 ==3)
       {
            operator=question("*");
       }
       else
       {
            operator=question("/");
       }
       return operator;
       
   }
   public static String question(String symbol)
   {
       return symbol;
   }

    public static int getLevel()
   {
       int choice4;
       int level;
        String [] menu4={"1)difficulty level 1","2)difficulty level 2","3)difficulty level 3"};
       choice4=displayMenu(menu4);
       
       if(choice4 ==1)
       {
           level=1;
       }
       else if(choice4 ==2)
       {
          level=2;
       }
       else
       {
           level=3;
       }
      return level;
   }
   public static void playGame(int level,String operator)
   {
      Scanner keyboard=new Scanner(System.in);
      int i=1;
      int question = 0;
      int number1;
      int number2;
      int ans;
      int points;
      int total;
      System.out.println("Game Start");
      while(i<5)
      {
          if (level == 1) {
              number1 = getRandomInt(1, 12);
              number2 = getRandomInt(1, 12);
              points=5;
          } else if (level == 2) {
              number1 = getRandomInt(13, 50);
              number2 = getRandomInt(13, 50);
              points=10;
          } else {
              number1 = getRandomInt(50, 100);
              number2 = getRandomInt(50, 100);
              points=20;
          }
          System.out.println("Question" + i);
          System.out.println(number1 + operator + number2);
          System.out.println("Please enter the answer:");
          question = keyboard.nextInt();
          ans=calculateAnswer(number1,number2,operator);
          
          if(question==ans)
          {
              total=getPoints(points);
             System.out.println("The answer is correct!");
             System.out.println(total);
          }
          else
          {
              System.out.println("The answer is wrong!");
              System.out.println("correct answer is "+ans);
          }
          i++;
        }
      
   }
   public static int calculateAnswer(int number1, int number2,String operator)
   {
       int correctAns;
       if(operator== "+")
       { correctAns=number1+number2;}
       else if(operator== "-")
       {correctAns=number1-number2;}
       else if(operator== "*")
       {correctAns=number1*number2;}
       else
       {correctAns=number1/number2;}
       return correctAns;
   }
           
   public static int getPoints(int points)
   {
       int totalPoint=0;
       System.out.println("Points awarded= "+points);
       totalPoint=totalPoint+points;
       return totalPoint;
   }
   public static void showPoints()
   {}
}
