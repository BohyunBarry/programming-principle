/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week1;
import java.util.Scanner;
/**
 *
 * @author User
 */
public class Ex2q4
{
 public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        //Declare variables
        int numJerseys;
        int type;
        int logo;
        final double LOGOPRICE = 1;
        final double COTTON = 12.5;
        final double SHORT = 15.5;
        final double LONG = 18.5;
        double logoprice;
        double total;
        double totalCost;

        //Input values
        System.out.print("Please enter number of jerseys:");
        numJerseys = keyboard.nextInt();
        System.out.println("Jersey Type; (1)Cotton Polo (2)Short Sleeve (3)Long Sleeve");
        System.out.print("Please enter jerseys type:");
        type = keyboard.nextInt();
        System.out.print("Logo? (1)Yes (2)No: ");
        logo = keyboard.nextInt();

        //Calculate
        if (logo == 1) 
        {
            logoprice = numJerseys * LOGOPRICE;
        } 
        else 
        {
            logoprice = 0;
        }
        
        if (type == 1) 
        {
            total = numJerseys * COTTON;

        } 
        else if (type == 2)
        {

            total = numJerseys * SHORT;

        } 
        else
        {

            total = numJerseys * LONG;

        }

        totalCost = logoprice + total;

        //Output Messages
        System.out.printf("Total Cost: €%.2f %n", totalCost);
    }
}
