/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week1;
import java.util.Scanner;
/**
 *
 * @author D00169978
 */
public class Ex1q2 
{
    public static void main (String[] args)
    {
        Scanner keyboard =new Scanner(System.in);
        //Declare variables
        int number1;
        int number2;
        int number3;
        double sum;
        double answer;
        
        //Input values
        System.out.print("Please enter number1:");
        number1=keyboard.nextInt();
        System.out.print("Please enter number2:");
        number2=keyboard.nextInt();
        System.out.print("Please enter number3:");
        number3=keyboard.nextInt();
        
        //Calculate
        sum= number1 + number2 + number3;
        answer=sum/3;
        
        //Output Messages
        System.out.printf("The average of 3 number is:%.2f %n",answer);
        
    }
}
