/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week1;
import java.util.Scanner;
/**
 *
 * @author D00169978
 */
public class Ex2q1 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        //Declare variables
        double exam;
        double ca;
        double total;
        final String PASS = "You're pass.";
        final String FAIL = "You're fail.";
        final int PASSMARK=40;
        
        //Input values
        System.out.print("Please enter exam mark:");
        exam = keyboard.nextDouble();
        System.out.print("Please enter continuous assessment mark:");
        ca = keyboard.nextDouble();

        //Calculate
        total = exam + ca;

        //Output Messages
        if (total >= PASSMARK) 
        {
            System.out.print("Your result is " + total + "\n" + PASS);
        } 
        else 
        {
            System.out.print("Your result is " + total + "\n" + FAIL);
        }

    }
}
