/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week1;
import java.util.Scanner;
/**
 *
 * @author User
 */
public class Ex1q3 
{
    public static void main (String[] args)
    {
        Scanner keyboard =new Scanner(System.in);
        //Declare variables
        double radius;
        double length;
        //final double PI=3.14;
        double area;
        double volume;
        
        //Input values
        System.out.print("Please enter radius of cylinder:");
        radius=keyboard.nextDouble();
        System.out.print("Please enter length of cylinder:");
        length=keyboard.nextDouble();
        
        
        //Calculate
        area= radius*radius*Math.PI;
        volume=area*length;
        
        //Output Messages
        System.out.printf("The area is:%.2f %n",area);
        System.out.printf("The volume is:%.2f %n",volume);
        
    }
    
}
