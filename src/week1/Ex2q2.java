/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package week1;

import java.util.Scanner;

/**
 *
 * @author D00169978
 */
public class Ex2q2 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        //Declare variables
        int age;
        int charge;

        //Input values
        System.out.print("Please enter your age:");
        age = keyboard.nextInt();

        //Calculate
        if (age < 5 ) 
        {
            charge = 0;
        } 
        else if (age >= 5 && age <= 18) 
        {
            charge = 10;
        } 
        else if (age >= 19 && age <= 65) 
        {
            charge = 15;
        } 
        else 
        {
            charge = 0;
        }

        //Output Messages
        System.out.println("The fee is €" + charge);

    }
}
