/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package week1;
import java.util.Scanner;
/**
 *
 * @author D00169978
 */
public class Ex2q3 
{
    public static void main(String[] args) 
    {
        Scanner keyboard = new Scanner(System.in);
        //Declare variables
        int hours;
        double regularpay;
        double overtimepay;
        double regularrate;
        double overtimerate;
        double total;
        final int BASICRATE=35;

        //Input values
        System.out.print("Please enter number hours of worked:");
        hours= keyboard.nextInt();
        System.out.print("Please enter regular hour rate");
        regularrate=keyboard.nextDouble();

        //Calculate
        
       
        if (hours <BASICRATE) 
        {
           regularpay=hours*regularrate;
           overtimepay=0;
           
        } 
        else
        {
            regularpay=BASICRATE*regularrate;
            overtimerate=regularrate*1.25;
            overtimepay=(hours-BASICRATE)*overtimerate;
           
        }
       total=regularpay+overtimepay;
    

        //Output Messages
        System.out.printf("Regular Pay: €%.2f %n",regularpay);
        System.out.printf("Overtime Pay: €%.2f %n",overtimepay);
        System.out.printf("Total Pay: €%.2f %n",total);
    }
}
